import java.util.*;

/* Application that takes the values of an array of ints and
 * concatenates them together, such that each int value is followed
 * by a space. 
 * @author gpainter@email.uscb.edu
 * @version CSCI 146 Spring 2018 Homework 4
 */
public class ConcatenateArrayTestApp {
    
    /**
     * Main method for the app
     * @param args 
     */
    public static void main( String[] args )
    {
        int[] myArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        
        // compute time for iterative version of algorithm
        long start = System.nanoTime();
        String iterativeResult = concatIterative( myArray );
        long end = System.nanoTime();
                
        System.out.println("Iterative results:");
        System.out.println("------------------");
        System.out.println("Concatenated array elements: " + iterativeResult);
        System.out.println("Time, in nanoseconds: " + (end - start) );
        
        // blank line for readability
        System.out.println();
        
        // compute time for recursive version of algorithm
        
        start = System.nanoTime();
        String recursiveResult = concatRecursive( myArray );
        end = System.nanoTime();
        
        System.out.println("Recursive results:");
        System.out.println("------------------");
        System.out.println("Concatenated array elements: " + recursiveResult);
        System.out.println("Time, in nanoseconds: " + (end - start) );
        
    
    } // end method main
    
    /**
     * Recursive method to concatenate the elements of an array of ints
     * @param arrayOfInts
     * @return the concatenation result
     */
    public static String concatRecursive( int[] arrayOfInts )
    {
        // INSTRUCTIONS: Replace the return statement below with code that
        // implements a recursive version of the concatenation algorithm
        // (NOTE: the final result returned by this method should be the same as
        //        what is produced by the concatIterative method below)
        if (arrayOfInts.length == 1) 
        {
            return arrayOfInts[0]+" ";
                    }
        else
        {
            return arrayOfInts[0] + " " + concatRecursive(Arrays.copyOfRange(arrayOfInts,1, arrayOfInts.length));
        }      
        //return "this is a dummy string";
        
    } // end method concatRecursive
    
    /**
     * Iterative method to concatenate the elements of an array of ints
     * @param arrayOfInts
     * @return the concatenation result
     */
    public static String concatIterative( int[] arrayOfInts )
    {
        String result = "";
        for ( int i = 0; i < arrayOfInts.length; i++ )
        {  
            result += arrayOfInts[i] + " ";
        } // end for
        return result;
        
    } // end method concatIterative
    
    
} // end class ConcatenateArrayTestApp